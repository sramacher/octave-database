#!/bin/sh

## This file is intended for package testers, to conveniently create
## an account and a test database in the local postgresql database
## cluster. Will su to root and then to user postgres. There may be a
## harmless warning that postgres can't cd to current directory.

## Register a local postgresql user name corresponding to the current
## system account name. Create a database 'test' owned by this
## user. If the user or the database already exist, postgresql will
## not recreate them, but notify you. If postgresql notifies you that
## database 'test' already exists, you may have to manually give the
## user 'create' priviledges on this database.
su -c "su -c '(echo \"create role $USER;\"; echo \"create database test with owner = $USER;\") | psql template1' postgres";

## For tests and demos to work, the local postgresql server has to be
## configured to trust local connections if the current system account
## name is equal to the requested postgresql user name. The following
## setting in /etc/postgresql/<version>/main/pg_hba.conf assures this:
##
##   # TYPE  DATABASE        USER        ADDRESS            METHOD
##
##   # "local" is for Unix domain socket connections only
##   local   all             all                            peer
##
##
## see:
##
## https://www.postgresql.org/docs/9.4/static/auth-methods.html#AUTH-PEER
##
## This configuration is _not_ provided by the current script, but
## should be the default at many systems.
